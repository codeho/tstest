import express from "express";

const app = express();

console.log("=============================================");
console.log(" ");
import dayjs from "dayjs";
import Web3 from "web3";
import { BN } from "ethereumjs-util";
import duration from "dayjs/plugin/duration";
import isBetween from "dayjs/plugin/isBetween";
import isLeapYear from "dayjs/plugin/isLeapYear";

const now = dayjs().unix();
const oneYearInSeconds = 31557600; // This is 365,25 Days in seconds.
const oneDayInSeconds = 86400;
const day0Unix = dayjs("2020-10-15T00:00:00.000Z").unix();

const scale = (x: number) => new BN((x * 1000000000000000).toString());
const unscale = (x: BN) => x.div(new BN("1000000000000000"));
const scaleAmt = new BN(1000000000000000);

const baseCirclesPerDayValue = 8;
let previousCirclesPerDayValue = 8;

const circlesValue = (x: number) => x * 1.07;
const lerp = (x: number, y: number, a: number) => x * (1 - a) + y * a;

function getBaseCirclesPerDayValue(yearsSince: number) {
  let circlesPerDayValue = baseCirclesPerDayValue;
  for (let index = 0; index < yearsSince; index++) {
    previousCirclesPerDayValue = circlesPerDayValue;
    circlesPerDayValue = circlesValue(circlesPerDayValue);
  }
  return circlesPerDayValue;
}

function convertTimeCirclesToCircles(amount: number, date: string) {
  const transactionDateUnix = date ? dayjs(date).unix() : now;
  const daysSinceDay0Unix = (transactionDateUnix - day0Unix) / oneDayInSeconds;
  const dayInCurrentCycle = Math.ceil(daysSinceDay0Unix % 365.25);
  const yearsSince = (transactionDateUnix - day0Unix) / oneYearInSeconds;
  const perDayValue = getBaseCirclesPerDayValue(yearsSince);
  // console.log("YEARS: ", yearsSinceDay0Unix);
  // console.log("\nAmount: ", amount);
  // console.log("\nDay in Current Cycle: ", dayInCurrentCycle);
  // console.log("\nActual Day Value: ", perDayValue);
  // console.log("\nPrevious Day Value: ", previousCirclesPerDayValue);

  return (
    (amount / 24) *
    lerp(previousCirclesPerDayValue, perDayValue, dayInCurrentCycle / 365.25)
  );
}

function convertCirclesToTimeCircles(amount: number, date: string) {
  const transactionDateUnix = date ? dayjs(date).unix() : now;
  const daysSinceDay0Unix = (transactionDateUnix - day0Unix) / oneDayInSeconds;
  const dayInCurrentCycle = Math.ceil(daysSinceDay0Unix % 365.25);
  const yearsSince = (transactionDateUnix - day0Unix) / oneYearInSeconds;
  const perDayValue = getBaseCirclesPerDayValue(yearsSince);
  // console.log("YEARS: ", yearsSinceDay0Unix);
  // console.log("yearsSince: ", yearsSince);
  // console.log("\nAmount: ", amount);

  // console.log("\nDay in Current Cycle: ", dayInCurrentCycle);
  // console.log("\nActual Day Value: ", perDayValue);
  // console.log("\nPrevious Day Value: ", previousCirclesPerDayValue);
  // const transactionDateUnix = dayjs("2021-10-16T00:00:00.000Z").unix();
  return (
    (amount /
      lerp(
        previousCirclesPerDayValue,
        perDayValue,
        dayInCurrentCycle / 365.25
      )) *
    24
  );
}

// console.log(
//   "\n\n8.56164052019165  Circles are: ",

//   convertCirclesToTimeCircles(8.56164052019165, "2021-10-16T00:00:00.000Z")
// );
// return Number.parseFloat(Web3.utils.fromWei(amount, "ether")).toFixed(2);
console.log(
  "\n6000 Time Circles are: ",

  convertTimeCirclesToCircles(6000, "2021-10-16T00:00:00.000Z")
);

// const s_day0: BN = scale(dayjs("2020-10-15T00:00:00.000Z").unix());
// const s_transactionDate: BN = scale(dayjs("2221-10-17T00:00:00.000Z").unix());
// const s_oneDay: BN = scale(86400);
// const days: BN = new BN(365);
// const s_oneYear: BN = s_oneDay.mul(days); // This is 365,25 Days in seconds.

// const s_overFlow: BN = scale(21600); // 0.25 days
// const s_timeSinceDay0 = new BN(s_transactionDate.sub(s_day0));

// console.log(
//   "\nseconds since day0",
//   Web3.utils.fromWei(s_timeSinceDay0).toString()
// );

// console.log("\ndays since day0", s_timeSinceDay0.div(s_oneDay).toString());

// console.log("\nyears since day0", s_timeSinceDay0.div(s_oneYear).toString());

// console.log(
//   "\ntransaction date seconds",
//   Web3.utils.fromWei(s_transactionDate).toString()
// );

// console.log("\nOne year: ", Web3.utils.fromWei(s_oneYear).toString());

// console.log("\nOne year in seconds", Web3.utils.fromWei(s_oneYear).toString());
// console.log("\nOne day in seconds", Web3.utils.fromWei(s_oneDay).toString());

// console.log("\ndays in one year", s_oneYear.div(s_oneDay).toString());

// const yearsSinceDay0 = s_timeSinceDay0.div(s_oneYear);

// // const dayInCurrentCycle = new BN(s_timeSinceDay0.mod(s_oneYear).div(s_oneDay));

// // If you set the
// console.log("\nday in current Cycle", dayInCurrentCycle.toString());

// console.log(
//   "asd",
//   Web3.utils.fromWei(yearsSinceDay0.mul(s_overFlow)).toString()
// );

// console.log(
//   "day in current Cycle",
//   Number.parseFloat(
//     Web3.utils.fromWei(dayInCurrentCycle.div(scaleAmt).toString(), "ether")
//   ).toFixed(2)
// );

// // Functions
// const circlesValue = (x: BN) => x.mul(new BN("1.07"));
// const lerp = (x: BN, y: BN, a: BN) => x.mul(new BN("1").sub(a).add(y.mul(a)));

// console.log("days", s_oneYear.div(oneDayInSeconds).div(scaleAmt).toString());

// console.log("OneLeapYear", s_oneYear.toString());
// console.log("current Day in Cycle", dayInCurrentCycle.toString());

// console.log("so: ", dayInCurrentCycle.toString());

// exit();

// function getBaseCirclesPerDayValue(yearsSince: number) {
//   let circlesPerDayValue = baseCirclesPerDayValue;
//   for (let index = 0; index < yearsSince; index++) {
//     circlesPerDayValue = circlesValue(circlesPerDayValue);
//     // console.log(`Per day year: ${index}`, circlesPerDayValue);
//   }
//   //   console.log("PERDAY: ", circlesPerDayValue);
//   return circlesPerDayValue;
// }

// function convertTimeCirclesToCircles(amount: BN, date: number) {
//   console.log("BN DINGS: ", amount);
//   console.log("DAte: ", date);

//   const yearsSince = Math.floor((date - day0Unix) / oneYearInSeconds);
//   const perDayValue = getBaseCirclesPerDayValue(yearsSince) * scale;

//   // return;

//   return amount
//     .div(new BN(baseTimeCircles))
//     .mul(
//       lerp(
//         new BN(baseCirclesPerDayValue),
//         new BN(perDayValue),
//         new BN(dayInCurrentCycle)
//       )
//     );
// }

// console.log(
//   "OH WEI",
//   convertTimeCirclesToCircles(
//     new BN(baseTimeCircles.toString()),
//     transactionDateUnix
//   ).toString()
// );

// console.log(
//   "BACK: ",
//   Number.parseFloat(
//     Web3.utils.fromWei(
//       convertTimeCirclesToCircles(new BN("24"), transactionDateUnix).toString(),
//       "ether"
//     )
//   ).toFixed(2)
// );

// function convertCirclesToTimeCircles(amount: number, date: number) {
// const yearsSince = Math.floor((date - day0Unix) / oneYearInSeconds);
// const perDayValue = getBaseCirclesPerDayValue(yearsSince);

// return (
//   (amount / lerp(baseCirclesPerDayValue, perDayValue, dayInCurrentCycle)) * 24
// );
// }

// Here we go.

// console.log(
//   `\n\nRight now 3000 Time Circles are ${convertTimeCirclesToCircles(
//     new BN("0.01"),
//     now
//   )} Circles`
// );

// console.log(
//   `\n\nRight now 1000 Circles are ${convertCirclesToTimeCircles(
//     1000,
//     now
//   )} Time Circles`
// );

// console.log("\n\nTransaction Date: ", `${transactionDate}\n\n`);
// console.log(
//   "Transaction Amount Circles : ",
//   `${transactionDateCirclesAmount}\n\n`
// );
// console.log(
//   "Transaction years since start (unix): ",
//   `${yearsSinceDay0Unix}\n\n`
// );
// console.log(
//   "Transaction days since start (unix): ",
//   `${daysSinceDay0Unix}\n\n`
// );

// const circlesPerDayValue = getBaseCirclesPerDayValue(yearsSinceDay0Unix);

// console.log(
//   "\n\nTransaction Amount Time Circles : ",
//   `${
//     convertCirclesToTimeCircles(transactionDateCirclesAmount, transactionDateUnix)
//   }\n\n`
// );

// console.log("DAY IN CURRENT CYCLE: ", dayInCurrentCycle);

/*
get transaction date
find beginning of current cycle date
find current day number within current cycle year
lerp:
x: circlesPerDayValueThisCycle
y: circlesPerDayValueNextCycle
a: currentDayInYearCycle / 365.25;
= circles amount for 24 Circles on that exact day.

*/

console.log(" ");
console.log("=============================================");

export default app;
